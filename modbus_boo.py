#!/usr/bin/env python

# pip install boofuzz
from boofuzz import *
import time

# Grammars:
#	A - 01, 02, 03, 04, 05, 06, 
#		Byte | Byte | 2 Bytes | 2 Bytes | 2 Bytes
#	B - 07, 11, 12, 17, 
#		Byte | Byte | 2 Bytes
#	C - 20, 
#		Byte | Byte | Byte | Byte | 2 Bytes | 2 Bytes | 2 Bytes | 2 Bytes
#	D - 43, 
#		Byte | Byte | Byte | N x Byte | 2 Bytes
#	E - 08, 
#		Byte | Byte | 2 Bytes | N x 2 Bytes | 2 Bytes
#	F - 15,
#		Byte | Byte | 2 Bytes | 2 Bytes | Byte | N x 1 Byte | 2 Bytes
#	G - 16, 
#		Byte | Byte | 2 Bytes | 2 Bytes | Byte | N x 2 Bytes | 2 Bytes
#	H - 21, 
#		Byte | Byte | Byte | Byte | 2 Bytes | 2 Bytes | 2 Bytes | N x 2 Bytes | 2 Bytes
#	I - 22,
#		Byte | Byte | 2 Bytes | 2 Bytes | 2 Bytes | 2 Bytes
#	J - 23,
#		Byte | Byte | 2 Bytes | 2 Bytes | 2 Bytes | 2 Bytes | Byte | N x 2 Bytes | 2 Bytes
#	K - 24,
# 		Byte | Byte | 2 Bytes | 2 Bytes

def main():
	session = Session(restart_sleep_time=0.2, crash_threshold=99999999999, target=Target(connection=SerialConnection(port="/dev/ttyUSB0", baudrate=9600)))
	
	s_initialize("RTU")
	with s_block("APDU"):
		s_static("\x05", name="unit_id")
		s_static("\x01", name="function_code")
		s_word(16385, endian=">", name="start_addr")
		s_word(5, endian=">", name="count")
	s_checksum("APDU", endian="<", algorithm="crc16-modbus")
		
	session.connect(s_get("RTU"))
	
	print("Mutations: " + str(s_num_mutations()))
	print("Execution Time: " + str(round(((s_num_mutations() * (0.2))/3600),2)) + " hours")	
	
	session.fuzz()
	helpers.pause_for_signal()
	
if __name__ == "__main__":
	main()
