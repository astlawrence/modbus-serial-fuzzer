Modbus serial fuzzer PoC.

Requires boofuzz (https://github.com/jtpereyda/boofuzz) and PyCRC (https://github.com/cristianav/PyCRC).

Replace boofuzz/blocks/checksum.py with the checksum.py file in this repo to add Modbus checksums to boofuzz.